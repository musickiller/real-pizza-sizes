import math

def circ(d):
    r = d/2
    return math.pi * (r ** 2)
    

# print(circ(33)*2)
# print(circ(30) + circ(40))
print(circ(25)*2)
print(circ(30))
print(circ(40))